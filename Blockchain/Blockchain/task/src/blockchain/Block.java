package blockchain;

import blockchain.utils.CryptoUtils;

import java.time.Instant;

public class Block {
    private int          id;
    private final long   timestamp;
    private String       previousHash;
    private final String hash;
    private final String magicNumber;

    public Block(int id,String prevHash,int difficulty) {
        this.id           = id;
        this.timestamp    = Instant.now().toEpochMilli();
        this.previousHash = prevHash;
        this.hash         = calculateHash();
        this.magicNumber  = calculateMagicNumber(difficulty);
    }

    public Block(int id,int difficulty){ //Start Block
        this.id           = id;
        this.timestamp    = Instant.now().toEpochMilli();
        this.previousHash = "0";
        this.hash         = calculateHash();
        this.magicNumber  = calculateMagicNumber(difficulty);
    }

    public int getId() {
        return id;
    }

    public Block setId(int id) {
        this.id = id;
        return this;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public String getHash() {
        return hash;
    }

    public String calculateHash(){
        return CryptoUtils.applySha256(this.id + this.previousHash + this.timestamp);
    }

    public String calculateMagicNumber(int difficulty){
        String target = new String(new char[difficulty]).replace('\0', '0'); //Create a string with difficulty * "0"
        int nonce = 0;
        while(!hash.substring( 0, difficulty).equals(target)) {
            nonce ++;
        }
        return String.valueOf(nonce);
    }
}
