package blockchain;

import blockchain.utils.CryptoUtils;

import java.util.ArrayList;
import java.util.List;

public class Blockchain {
    final List<Block> blockchain = new ArrayList<>();

    public boolean generate(int difficulty) {
        Block block;
        Block prevBlock;

        if (blockchain.isEmpty()){
            block = new Block(1,difficulty);
        }else{
            int lastindex = blockchain.size() - 1;

            prevBlock = blockchain.get(lastindex);
            block     = new Block(prevBlock.getId()+1,prevBlock.getHash(),difficulty);
        }

        return blockchain.add(block);
    }

    public boolean validate() {
        String prevHash = "0";
        for (Block block : blockchain) {
            String id        = String.valueOf(block.getId());
            String timestamp = String.valueOf(block.getTimestamp());
            String hash      = block.getHash();

            String validHash = CryptoUtils.applySha256(id + prevHash + timestamp);

            if (!hash.equals(validHash))
                return false;
            prevHash = hash;
        }
        return true;
    }

    public List<Block> getBlockchain() {
        return blockchain;
    }

    @Override
    public String toString() {
        String message = "";
        for (Block block: blockchain){
            message += "Block:\n" +
                    "Id:" + block.getId() + "\n" +
                    "Timestamp:" + block.getTimestamp() + "\n" +
                    "Hash of the previous block:\n" +
                    block.getPreviousHash() + "\n" +
                    "Hash of the block:\n" +
                    block.getHash() + "\n\n";
        }
        return message;
    }
}
