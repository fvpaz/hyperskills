package recognition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    private final static int[] weight = {2, 1, 2, 4, -4, 4, 2, -1, 2, -5};

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int[] row1, row2, row3;

        List<Integer> grid = new ArrayList<>();
        try {
            System.out.println("Input grid:");
            grid.addAll(getRowNeuron(scanner.nextLine()));
            grid.addAll(getRowNeuron(scanner.nextLine()));
            grid.addAll(getRowNeuron(scanner.nextLine()));

            int number = calcul(grid.toArray(new Integer[0]));

            if (number < 0) {
                number = 0;
            } else if (number >= 0 && number <= 1) {
                number = 1;
            } else if (number > 8)
                number = 9;

            System.out.printf("This number is " + number);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int calcul(Integer[] a) {
        int bias = 1 * weight[9];
        int total = a[0] * weight[0] +
                a[1] * weight[1] +
                a[2] * weight[2] +
                a[3] * weight[3] +
                a[4] * weight[4] +
                a[5] * weight[5] +
                a[6] * weight[6] +
                a[7] * weight[7] +
                a[8] * weight[8] +
                bias;

        return total > 0 ? 0 : 1;
    }

    public static List<Integer> getRowNeuron(String rowNeuron) throws Exception {
        List<Integer> row = new ArrayList<>();

        char[] rowChar = rowNeuron.toCharArray();

        if (rowChar.length != 3) {
            throw new Exception("Invalid legth row");
        }

        for (int i = 0; i < rowChar.length; i++) {
            int value = rowChar[i] == 'X' ? 1 : 0;
            row.add(value);
        }

        return row;
    }
}
