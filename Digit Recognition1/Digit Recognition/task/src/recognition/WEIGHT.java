package recognition;

public enum WEIGHT {
    w1(2),
    w2(1),
    w3(2),
    w4(4),
    w5(-4),
    w6(4),
    w7(2),
    w8(-1),
    w9(2), b(-5);

    private int value;

    WEIGHT(int value) {
        this.value = value;
    }
}
