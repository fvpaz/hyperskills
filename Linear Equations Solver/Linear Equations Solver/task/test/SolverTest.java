import org.hyperskill.hstest.v1.CheckResult;
import org.hyperskill.hstest.v1.MainMethodTest;
import org.hyperskill.hstest.v1.TestCase;

import solver.Main;

import static java.lang.Math.abs;


class TestClue {

    TestClue(){

    }
}


public class SolverTest extends MainMethodTest<TestClue> {

    public SolverTest() throws Exception {
        super(Main.class);
    }

    @Override
    public TestCase[] generateTestCases() {
        return new TestCase[] {
            new TestCase<>(null, "5 3"),
            new TestCase<>(null, "12 67"),
            new TestCase<>(null, "12 12"),
            new TestCase<>(null, "-1 1"),
            new TestCase<>(null, "2 -3"),
            new TestCase<>(null, "2.34 12.23"),
        };
    }

    @Override
    public String solve(String input) {
        String[] nums = input.split(" ");
        double a = Double.parseDouble(nums[0]);
        double b = Double.parseDouble(nums[1]);
        return String.valueOf(b / a);
    }

    @Override
    public CheckResult checkSolved(String reply, String clue) {
        try {
            double actual = Double.parseDouble(reply);
            double expected = Double.parseDouble(clue);
            return new CheckResult(abs(actual - expected) < 0.001);
        }
        catch (Exception ex) {
            return new CheckResult(false, "Can't check the answer");
        }
    }
}
